#pragma once

#include "Config.h"

struct Gravity
{
	explicit Gravity(float f = Config::gravity_constant);
	// ~Gravity()

	float	force;
};