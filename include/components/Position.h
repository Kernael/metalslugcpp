#pragma once

struct Position
{
	explicit Position(float x = 0.0, float y = 0.0, float d_x = 0.0, float d_y = 0.0);

	float	x, y;
	float	direct_x, direct_y;
};