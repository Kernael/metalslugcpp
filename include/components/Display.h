#pragma once

#include <SFML/Graphics.hpp>

struct Display
{
	Display();
	explicit Display(const sf::IntRect& coord);
	Display(const sf::IntRect& coord, const sf::Color& color);

	sf::IntRect	coord;
	sf::Color	color;
};