#pragma once

#include <entityx/Entity.h>
#include <SFML/Graphics.hpp>

struct KeyEvent
{
	explicit KeyEvent(const entityx::Entity& entity, const sf::Keyboard::Key& key);
	// ~KeyEvent();

	entityx::Entity		entity;
	sf::Keyboard::Key	key;
};