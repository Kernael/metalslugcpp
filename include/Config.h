#pragma once

#include <string>
#include <SFML/Graphics.hpp>
#include <map>

namespace Config
{
	static const int	window_width = 800;
	static const int	window_height = 600;

	static const int	pixels_per_cell = 30;

	static const float	ms_per_update = 10.0;

	static const float	gravity_constant = 8.0;
	static const float	jump_constant = 2.0;
	static const float	move_constant = 10.0;

	static const float	jump_timer = 0.5;

	static const std::string	ressource_dir = "../ressources/";

	static const std::map<std::string, sf::IntRect>	sprite_map = {
			{ "player", { 22, 30, 71, 179 } },
			{ "ak47", { 181, 105, 143, 44 } }
	};
}