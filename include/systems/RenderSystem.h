#pragma once

#include <entityx/System.h>
#include <SFML/Graphics.hpp>
#include <memory>
#include "components/Display.h"
#include "components/Position.h"

class RenderSystem : public entityx::System < RenderSystem >
{
public:
	RenderSystem(
		sf::RenderWindow& window,
		const std::shared_ptr<sf::Texture>& spSpriteSheet
		);
	virtual ~RenderSystem() override {};

	void update(
		entityx::EntityManager& entities,
		entityx::EventManager& events,
		entityx::TimeDelta dt
		) override;

	bool isOutsideScreen(
		const entityx::ComponentHandle<Position>& pos,
		const entityx::ComponentHandle<Display>& display
		) const;

private:
	sf::RenderWindow&	m_window;
	std::shared_ptr<sf::Texture>	m_spSpriteSheet;
	sf::Sprite	m_sprite;
};