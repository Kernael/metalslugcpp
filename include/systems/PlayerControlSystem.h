#pragma once

#include <entityx/System.h>
#include <SFML/Graphics.hpp>

class PlayerControlSystem : public entityx::System < PlayerControlSystem >
{
public:
	PlayerControlSystem();
	virtual ~PlayerControlSystem() override {};

	void update(
		entityx::EntityManager& entities,
		entityx::EventManager& events,
		entityx::TimeDelta dt
		) override;

private:
	std::vector<sf::Keyboard::Key>	m_controls;
};