#pragma once

#include <entityx/System.h>
#include "events/KeyEvent.h"
#include "components/Position.h"
#include "components/Speed.h"

class MovementSystem :
	public entityx::System<MovementSystem>,
	public entityx::Receiver < MovementSystem >
{
public:
	MovementSystem();
	virtual ~MovementSystem() override {};

	virtual void configure(entityx::EventManager& events) override;
	virtual void update(
		entityx::EntityManager& entities,
		entityx::EventManager& events,
		entityx::TimeDelta dt
		) override;

	void receive(const KeyEvent& jump);

	static void move_entity(
		entityx::ComponentHandle<Position>& position,
		entityx::ComponentHandle<Speed>& speed,
		entityx::TimeDelta dt
		);
	static void	move_attachements(
		entityx::Entity parent,
		const entityx::ComponentHandle<Position>& position
		);

private:
	float	m_jumpTimer;
};