#pragma once

#include <SFML/Graphics.hpp>
#include <entityx/entityx.h>

class Game
{
public:
	explicit Game(sf::RenderWindow& window);
	// ~Game();

	void	run();
	void	processEvents();
	void	update(float dt);
	void	render();

private:
	sf::RenderWindow&	m_window;

	entityx::EventManager	m_eventManager;
	entityx::EntityManager	m_entityManager;
	entityx::SystemManager	m_systemManager;

	entityx::Entity	m_playerEntity;
};