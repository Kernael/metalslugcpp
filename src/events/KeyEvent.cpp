#include <events/KeyEvent.h>

KeyEvent::KeyEvent(
	const entityx::Entity& ent,
	const sf::Keyboard::Key& k
	) : entity(ent), key(k)
{
}