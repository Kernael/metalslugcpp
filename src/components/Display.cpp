#include "components/Display.h"

Display::Display() : Display(sf::IntRect())
{
}

Display::Display(const sf::IntRect& coord) : Display(coord, sf::Color())
{
}

Display::Display(const sf::IntRect& coord, const sf::Color& color)
	: coord(coord), color(color)
{
}