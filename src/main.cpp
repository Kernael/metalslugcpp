#include <iostream>
#include "Game.h"
#include "Config.h"

int	main()
{
	try
	{
		sf::RenderWindow	window(sf::VideoMode(
			Config::window_width,
			Config::window_height
			), "Metal slug");

		window.setVerticalSyncEnabled(true);

		Game	game(window);

		game.run();
	}
	catch (const std::exception& e)
	{
		std::cerr << "Standard exception catched : " << e.what() << std::endl;
		return 1;
	}
	catch (...)
	{
		std::cerr << "Unknown exception catched" << std::endl;
		return 1;
	}

	return 0;
}