#include "Game.h"
#include "systems/RenderSystem.h"
#include "systems/MovementSystem.h"
#include "systems/PlayerControlSystem.h"
#include "components/PlayerControl.h"
#include "components/Gravity.h"
#include "components/Speed.h"
#include "components/Attachement.h"
#include "Config.h"

Game::Game(sf::RenderWindow& window)
	: m_window(window),
	m_eventManager(),
	m_entityManager(m_eventManager),
	m_systemManager(m_entityManager, m_eventManager)
{
	auto	sp_sprite_sheet = std::make_shared<sf::Texture>();
	sp_sprite_sheet->loadFromFile(Config::ressource_dir + "spritesheet.png");
	sp_sprite_sheet->setSmooth(true);

	m_systemManager.add<RenderSystem>(m_window, sp_sprite_sheet);
	m_systemManager.add<MovementSystem>();
	m_systemManager.add<PlayerControlSystem>();
	m_systemManager.configure();

	m_playerEntity = m_entityManager.create();
	m_playerEntity.assign<Position>(0, 1);
	m_playerEntity.assign<Display>(Config::sprite_map.at("player"));
	m_playerEntity.assign<PlayerControl>();
	m_playerEntity.assign<Gravity>();
	m_playerEntity.assign<Speed>(180.0, 10.0);
	m_playerEntity.assign<Attachement>();

	auto	default_weapon = m_entityManager.create();

	default_weapon.assign<Display>(Config::sprite_map.at("ak47"));
	default_weapon.assign<Position>(
		m_playerEntity.component<Position>()->x,
		m_playerEntity.component<Position>()->y,
		25.0,
		0.0
		);

	m_playerEntity.component<Attachement>()->attachements.insert(
		std::make_pair("weapon", default_weapon)
		);
}

void Game::run()
{
	sf::Clock	clock;

	while (m_window.isOpen())
	{
		float	dt = clock.restart().asSeconds();

		std::cout << 1.0 / dt << std::endl;

		this->processEvents();

		this->update(dt);

		this->render();
	}
}

void Game::processEvents()
{
	sf::Event	event;

	while (m_window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			m_window.close();
			break;
		default:
			break;
		}
	}
}

void Game::update(float dt)
{
	m_systemManager.update<PlayerControlSystem>(dt);
	m_systemManager.update<MovementSystem>(dt);
}

void Game::render()
{
	m_systemManager.update<RenderSystem>(0.0);

	m_window.display();
}