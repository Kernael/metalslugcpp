#include "systems/RenderSystem.h"
#include "Config.h"

RenderSystem::RenderSystem(
	sf::RenderWindow& window,
	const std::shared_ptr<sf::Texture>& spSpriteSheet
	)
	: m_window(window),
	m_spSpriteSheet(spSpriteSheet),
	m_sprite(*m_spSpriteSheet)
{
}

void RenderSystem::update(
	entityx::EntityManager& entities,
	entityx::EventManager& events,
	entityx::TimeDelta dt
	)
{
	m_window.clear();

	entityx::ComponentHandle<Display>		display;
	entityx::ComponentHandle<Position>		position;

	for (auto entity : entities.entities_with_components(position, display))
	{
		if (!isOutsideScreen(position, display))
		{
			m_sprite.setOrigin(
				display->coord.width / 2,
				display->coord.height / 2
				);
			m_sprite.setPosition(
				position->x,
				Config::window_height - (position->y * Config::pixels_per_cell)
				);
			m_sprite.setTextureRect(display->coord);
			//m_sprite.setColor(display->color);
			m_sprite.setScale(0.5, 0.5);

			m_window.draw(m_sprite);
		}
	}
}

bool RenderSystem::isOutsideScreen(
	const entityx::ComponentHandle<Position>& pos,
	const entityx::ComponentHandle<Display>& display
	) const
{
	return (pos->x + display->coord.width / 2.0 < 0.0)
		|| (pos->x - display->coord.width / 2.0 > m_window.getSize().x)
		|| (pos->y - display->coord.height / 2.0 > m_window.getSize().y);
}