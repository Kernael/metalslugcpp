#include "systems/MovementSystem.h"
#include "components/Gravity.h"
#include "components/Attachement.h"

MovementSystem::MovementSystem() : m_jumpTimer(0.0)
{
}

void MovementSystem::configure(entityx::EventManager& events)
{
	events.subscribe<KeyEvent>(*this);
}

void MovementSystem::update(
	entityx::EntityManager& entities,
	entityx::EventManager& events,
	entityx::TimeDelta dt
	)
{
	entityx::ComponentHandle<Position>	position;
	entityx::ComponentHandle<Speed>		speed;

	if (m_jumpTimer) { m_jumpTimer -= dt; }

	for (auto entity : entities.entities_with_components(position, speed))
	{
		move_entity(position, speed, dt);

		if (entity.has_component<Gravity>() && position->y > 1.0 &&
			position->direct_y < 1.0)
		{
			position->y -= entity.component<Gravity>()->force * dt;
			if (position->y < 0) { position->y = 0; }
		}

		if (entity.has_component<Attachement>())
		{
			move_attachements(entity, position);
		}
	}
}

void MovementSystem::receive(const KeyEvent& event)
{
	auto entity = event.entity;
	auto key = event.key;
	auto pos = entity.component<Position>();

	switch (key)
	{
	case sf::Keyboard::Up:
		if (m_jumpTimer <= 0.0)
		{
			pos->direct_y = pos->y + Config::jump_constant;
			m_jumpTimer = Config::jump_timer;
		}
		break;
	case sf::Keyboard::Right:
		pos->direct_x = pos->x + Config::move_constant;
		break;
	case sf::Keyboard::Left:
		pos->direct_x = pos->x - Config::move_constant;
		break;
	default:
		break;
	}
}

void MovementSystem::move_entity(
	entityx::ComponentHandle<Position>& position,
	entityx::ComponentHandle<Speed>& speed,
	entityx::TimeDelta dt
	)
{
	std::pair<float, float>	move = {
		speed->x * dt,
		speed->y * dt
	};

	if (position->direct_x)
	{
		if (position->direct_x > position->x)
		{
			position->x += move.first;
			if (position->x >= position->direct_x) { position->direct_x = 0.0; }
		}
		else if (position->direct_x < position->x)
		{
			position->x -= move.first;
			if (position->direct_x <= position->direct_x) { position->direct_x = 0.0; }
		}
	}

	if (position->direct_y)
	{
		if (position->direct_y > position->y)
		{
			position->y += move.second;
			if (position->y >= position->direct_y) { position->direct_y = 0.0; }
		}
		else if (position->direct_y < position->y)
		{
			position->y -= move.second;
			if (position->y <= position->direct_y) { position->direct_y = 0.0; }
		}
	}
}

void MovementSystem::move_attachements(
	entityx::Entity parent,
	const entityx::ComponentHandle<Position>& position
	)
{
	for (auto entity : parent.component<Attachement>()->attachements)
	{
		if (entity.second.has_component<Position>())
		{
			entityx::ComponentHandle<Position>	entity_pos =
				entity.second.component<Position>();

			entity_pos->x = position->x + entity_pos->direct_x;
			entity_pos->y = position->y + entity_pos->direct_y;
		}
	}
}