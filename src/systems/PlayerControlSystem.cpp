#include "systems/PlayerControlSystem.h"
#include "components/PlayerControl.h"
#include "events/KeyEvent.h"
#include "Config.h"
#include <algorithm>

PlayerControlSystem::PlayerControlSystem()
{
	m_controls = {
		sf::Keyboard::Left,
		sf::Keyboard::Right,
		sf::Keyboard::Up,
		sf::Keyboard::Space
	};
}

void PlayerControlSystem::update(
	entityx::EntityManager& entities,
	entityx::EventManager& events,
	entityx::TimeDelta dt
	)
{
	entityx::ComponentHandle<PlayerControl>	controller;

	std::vector<bool>	keyPressed(m_controls.size(), false);

	for (size_t i = 0; i < m_controls.size(); ++i)
	{
		if (sf::Keyboard::isKeyPressed(m_controls[i]))
		{
			keyPressed[i] = true;
		}
	}

	if (!std::count(keyPressed.begin(), keyPressed.end(), true)) { return; }

	for (auto entity : entities.entities_with_components(controller))
	{
		for (size_t i = 0; i < m_controls.size(); ++i)
		{
			if (keyPressed[i])
			{
				events.emit<KeyEvent>(entity, m_controls[i]);
			}
		}
	}
}