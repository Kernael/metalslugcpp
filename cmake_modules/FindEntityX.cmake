
find_path(ENTITYX_INCLUDE_DIR entityx/entityx.h
   PATHS
   ${PROJECT_SOURCE_DIR}/lib/
)

find_library(ENTITYX_LIBRARY_DEBUG
	NAMES entityx-d libentityx
	HINTS
	${PROJECT_SOURCE_DIR}/lib/entityx/build/Debug/
	)
find_library(ENTITYX_LIBRARY_RELEASE
	NAMES entityx libentityx
	HINTS
	${PROJECT_SOURCE_DIR}/lib/entityx/build/Release/
	)

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(EntityX  
								DEFAULT_MSG
								ENTITYX_LIBRARY_DEBUG
								ENTITYX_LIBRARY_RELEASE
								ENTITYX_INCLUDE_DIR)